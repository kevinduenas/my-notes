import React from 'react';

import './App.css';
import {  Query } from 'react-apollo';
import {gql} from "apollo-boost";
import {Button} from "antd";

const  CharacterQuery = () => {
  return (<Query query = {gql`{
    users{
      lastName
      firstName
    }
  }`
}
  >
    {({loading, error, data})=>{
      if(loading)return <p> Loading...</p>
      if(error)return <p>ERROR!</p>

      return data.users.map(users => {
      return <p>{users.lastName}</p>
      })
    }}
  </Query>
  );   
};

function App() {
  return(
   
  <div className="App" >
    <CharacterQuery />
    <Button type="Secundary">Kevin</Button>
  </div>
  );
}

export default App;
