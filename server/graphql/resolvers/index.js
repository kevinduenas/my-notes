
//user
import userMutations from "./user/mutations.js";
import userQueries from "./user/queries.js";

const resolvers = {
    Query:{
        ...userQueries
    },
    Mutation:{
        ...userMutations
    }
};

export default resolvers;
