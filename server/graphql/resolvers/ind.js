import {Cat} from "../../mongo-db/models/Cat.js"

const resolvers = {
    Query: {
      books: () => books,
      clients: ()=> clients,
      cats: () => Cat.find()
    },
    Mutation: {
      createCat: async (_, {name}) => {
        const kitty = new Cat({name});
        await  kitty.save();
        return kitty;

      }
      
      
    }

  };

  export default resolvers;
  const books = [
    {
      title: 'Harry Potter and the Chamber of Secrets',
      author: 'J.K. Rowling',
    },
    {
      title: 'Jurassic Park',
      author: 'Michael Crichton',
    },
  ];

  const clients = [
    {
      name: 'Kevin Duenas',
      age: '19',
      sex: 'Male',
    }
  ]