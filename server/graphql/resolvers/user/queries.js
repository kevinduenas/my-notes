import User from "../../../mongo-db/models/User";

const userQueries = {
    users: () => User.find(),
    user: (id) => User.findOne(id)
}

export default userQueries;