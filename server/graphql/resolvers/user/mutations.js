import User from "../../../mongo-db/models/User";

const userMutations = {
    user: async (_, {args }) => {
        const user = new User(...args.user);
            await  user.save();
            return user;
      }
}

export default userMutations;