import mongoose, {model, Schema} from "mongoose";

const User = new Schema(
    {
        firstName: { type: String, required: true },
        lastName: { type: String, required: true },
        username: { type: String, trim: true, index: true, unique: true, sparse: true },
        email: { type: String, trim: true, index: true, unique: true, sparse: true },
        password: { type: String, required: true },
        profileImage: { type: String, required: false },
        active: { type: Boolean, required: true, default: true }
},)

export default model("User", User);