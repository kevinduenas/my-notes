import path from 'path';
import { ApolloServer } from "apollo-server";
import { importSchema } from "graphql-import";
import resolvers from "./server/graphql/resolvers/index.js";
import  mongoose from "mongoose";
//import typedefs from "./server/graphql/typedefs/index.graphql"

const typeDefs = importSchema(path.join(__dirname, './server/graphql/typedefs/index.graphql'));

const startServer = async () =>{
    const server = new ApolloServer ({typeDefs, resolvers});
    
    
    await mongoose.connect("mongodb://localhost:27017/my-notes-db",{
      useNewUrlParser: true
   });
   
   server.listen().then(({ url }) => {
       console.log(`🚀  Server ready at ${url}`);
   });

}
        
startServer();

    



